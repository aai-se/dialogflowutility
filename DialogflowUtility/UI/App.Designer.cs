﻿namespace Com.AutomationAnywhere.Utility.Dialogflow.UI
{
    partial class App
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtControlRoomUrl = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnCopy = new System.Windows.Forms.Button();
            this.btnFetchModel = new System.Windows.Forms.Button();
            this.txtWorkModel = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnFetchQueue = new System.Windows.Forms.Button();
            this.cmbQueue = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.rtbDFCode = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.rdbDF = new System.Windows.Forms.RadioButton();
            this.rdbAWS = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtPassword);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtUserName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtControlRoomUrl);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(943, 120);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // txtPassword
            // 
            this.txtPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPassword.Location = new System.Drawing.Point(121, 87);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(702, 22);
            this.txtPassword.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Password";
            // 
            // txtUserName
            // 
            this.txtUserName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUserName.Location = new System.Drawing.Point(121, 57);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(702, 22);
            this.txtUserName.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "User Name";
            // 
            // txtControlRoomUrl
            // 
            this.txtControlRoomUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtControlRoomUrl.Location = new System.Drawing.Point(121, 26);
            this.txtControlRoomUrl.Name = "txtControlRoomUrl";
            this.txtControlRoomUrl.Size = new System.Drawing.Size(799, 22);
            this.txtControlRoomUrl.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Control Room Url";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.rdbAWS);
            this.groupBox2.Controls.Add(this.rdbDF);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.btnCopy);
            this.groupBox2.Controls.Add(this.btnFetchModel);
            this.groupBox2.Controls.Add(this.txtWorkModel);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.btnFetchQueue);
            this.groupBox2.Controls.Add(this.cmbQueue);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(12, 123);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(940, 149);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // btnCopy
            // 
            this.btnCopy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCopy.Location = new System.Drawing.Point(829, 86);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(105, 27);
            this.btnCopy.TabIndex = 6;
            this.btnCopy.Text = "Copy Code";
            this.btnCopy.UseVisualStyleBackColor = true;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // btnFetchModel
            // 
            this.btnFetchModel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFetchModel.Location = new System.Drawing.Point(829, 51);
            this.btnFetchModel.Name = "btnFetchModel";
            this.btnFetchModel.Size = new System.Drawing.Size(105, 27);
            this.btnFetchModel.TabIndex = 5;
            this.btnFetchModel.Text = "Fetch Model";
            this.btnFetchModel.UseVisualStyleBackColor = true;
            this.btnFetchModel.Click += new System.EventHandler(this.btnFetchModel_Click);
            // 
            // txtWorkModel
            // 
            this.txtWorkModel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWorkModel.Location = new System.Drawing.Point(121, 50);
            this.txtWorkModel.Multiline = true;
            this.txtWorkModel.Name = "txtWorkModel";
            this.txtWorkModel.ReadOnly = true;
            this.txtWorkModel.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtWorkModel.Size = new System.Drawing.Size(702, 63);
            this.txtWorkModel.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 16);
            this.label5.TabIndex = 3;
            this.label5.Text = "Work Model";
            // 
            // btnFetchQueue
            // 
            this.btnFetchQueue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFetchQueue.Location = new System.Drawing.Point(829, 18);
            this.btnFetchQueue.Name = "btnFetchQueue";
            this.btnFetchQueue.Size = new System.Drawing.Size(105, 27);
            this.btnFetchQueue.TabIndex = 2;
            this.btnFetchQueue.Text = "Fetch Queues";
            this.btnFetchQueue.UseVisualStyleBackColor = true;
            this.btnFetchQueue.Click += new System.EventHandler(this.btnFetchQueue_Click);
            // 
            // cmbQueue
            // 
            this.cmbQueue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbQueue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbQueue.FormattingEnabled = true;
            this.cmbQueue.Location = new System.Drawing.Point(121, 18);
            this.cmbQueue.Name = "cmbQueue";
            this.cmbQueue.Size = new System.Drawing.Size(702, 24);
            this.cmbQueue.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Work Queues";
            // 
            // btnGenerate
            // 
            this.btnGenerate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenerate.Location = new System.Drawing.Point(7, 278);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(945, 30);
            this.btnGenerate.TabIndex = 2;
            this.btnGenerate.Text = "Generate Code";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // rtbDFCode
            // 
            this.rtbDFCode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbDFCode.Location = new System.Drawing.Point(7, 315);
            this.rtbDFCode.Name = "rtbDFCode";
            this.rtbDFCode.ReadOnly = true;
            this.rtbDFCode.Size = new System.Drawing.Size(945, 435);
            this.rtbDFCode.TabIndex = 3;
            this.rtbDFCode.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 121);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 16);
            this.label6.TabIndex = 7;
            this.label6.Text = "Generate code for";
            // 
            // rdbDF
            // 
            this.rdbDF.AutoSize = true;
            this.rdbDF.Checked = true;
            this.rdbDF.Location = new System.Drawing.Point(127, 120);
            this.rdbDF.Name = "rdbDF";
            this.rdbDF.Size = new System.Drawing.Size(91, 20);
            this.rdbDF.TabIndex = 8;
            this.rdbDF.TabStop = true;
            this.rdbDF.Text = "Dialogflow";
            this.rdbDF.UseVisualStyleBackColor = true;
            // 
            // rdbAWS
            // 
            this.rdbAWS.AutoSize = true;
            this.rdbAWS.Location = new System.Drawing.Point(240, 121);
            this.rdbAWS.Name = "rdbAWS";
            this.rdbAWS.Size = new System.Drawing.Size(112, 20);
            this.rdbAWS.TabIndex = 9;
            this.rdbAWS.TabStop = true;
            this.rdbAWS.Text = "AWS Lambda";
            this.rdbAWS.UseVisualStyleBackColor = true;
            // 
            // App
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(964, 754);
            this.Controls.Add(this.rtbDFCode);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "App";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "App";
            this.Load += new System.EventHandler(this.App_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtControlRoomUrl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnFetchQueue;
        private System.Windows.Forms.ComboBox cmbQueue;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnFetchModel;
        private System.Windows.Forms.TextBox txtWorkModel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.RichTextBox rtbDFCode;
        private System.Windows.Forms.Button btnCopy;
        private System.Windows.Forms.RadioButton rdbAWS;
        private System.Windows.Forms.RadioButton rdbDF;
        private System.Windows.Forms.Label label6;
    }
}