﻿using Com.AutomationAnywhere.Utility.Dialogflow.Util;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Com.AutomationAnywhere.Utility.Dialogflow.UI
{
    public partial class App : Form
    {
        string token = string.Empty;
        const string WINDOWTITLE = "Automation Anywhere Code Generator";

        public App()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Text = WINDOWTITLE;
        }

        private async void App_Load(object sender, EventArgs e)
        {
            await assignTextAsync(txtControlRoomUrl, "https://aa-se-ind-5.my.automationanywhere.digital");
            await assignTextAsync(txtUserName, "himanshum");
            await assignTextAsync(txtPassword, "Admin@12345");
        }

        private async void btnFetchQueue_Click(object sender, EventArgs e)
        {
            try
            {
                token = await ControlRoomAPIUtil.Instance.GetAccessTokenAsync(txtControlRoomUrl.Text.Trim(), txtUserName.Text.Trim(), txtPassword.Text.Trim());
                if (token != null)
                {
                    var items = await ControlRoomAPIUtil.Instance.GetWorkQueuesAsync(txtControlRoomUrl.Text.Trim(), token);
                    await fillWorkItemComboAsync(items);
                }
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Invalid Credentials!", WINDOWTITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }catch (Exception)
            {
                MessageBox.Show("Oops! something goes wrong.", WINDOWTITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private async Task fillWorkItemComboAsync(IEnumerable<ComboItem> items)
        {
            await Task.Run(() =>
            {
                if (cmbQueue.InvokeRequired)
                {
                    Action safeAction = () => fillWorkItemCombo(items);
                    cmbQueue.Invoke(safeAction);
                }
                else
                {
                    fillWorkItemCombo(items);
                }
            });
        }

        private void fillWorkItemCombo(IEnumerable<ComboItem> items)
        {
            cmbQueue.Items.Clear();
            foreach (ComboItem item in items)
            {
                cmbQueue.Items.Add(item);
            }
        }

        private async void btnFetchModel_Click(object sender, EventArgs e)
        {
            var item = cmbQueue.SelectedItem as ComboItem;
            await assignTextAsync(txtWorkModel,  await ControlRoomAPIUtil.Instance.GetWorkQueueModelAsync(txtControlRoomUrl.Text.Trim(), token, item.WorkItemId));
        }

        private void assignText(Object sender, String text)
        {
            ((TextBox)sender).Text = text;
        }

        private async Task assignTextAsync(Object sender, String workModel)
        {
            await Task.Run(() => 
            {
                var txtSender = sender as TextBox;
                if (txtSender.InvokeRequired)
                {
                    Action action = () => assignText(txtSender, workModel);
                    txtSender.Invoke(action);
                }
                else
                {
                    assignText(txtSender, workModel);
                }
            });
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            var item = cmbQueue.SelectedItem as ComboItem;
            if (item != null)
            {
                if (rdbDF.Checked)
                {
                    rtbDFCode.Text = generateDialogflowCode(txtControlRoomUrl.Text.Trim(),
                                                            txtUserName.Text.Trim(),
                                                            txtPassword.Text.Trim(),
                                                            item.Id,
                                                            txtWorkModel.Text.Trim());
                }
                else
                {
                    rtbDFCode.Text = generateAWSLambdaCode(txtControlRoomUrl.Text.Trim(),
                                                           txtUserName.Text.Trim(),
                                                           txtPassword.Text.Trim(),
                                                           item.Id,
                                                           txtWorkModel.Text.Trim());
                }
            }
        }

        private static String generateDialogflowCode(String controlRoomUrl, String userName, String password, String queueID, String workitemModel)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("'use strict'");
            builder.AppendLine("");
            builder.AppendLine("var https = require('https');");
            builder.AppendLine($"var controlRoomURL = \"{controlRoomUrl}\";");
            builder.AppendLine($"var username = \"{userName}\";");
            builder.AppendLine($"var password = \"{password}\";");
            builder.AppendLine($"var queueID = '{queueID}';");
            builder.AppendLine($"var data_template = \"{workitemModel.Replace("\"", "\\\"")}\";");
            builder.AppendLine("");
            builder.AppendLine("const functions = require('firebase-functions');");
            builder.AppendLine("const DialogFlowApp = require('actions-on-google').DialogFlowApp;");
            builder.AppendLine("");
            builder.AppendLine("var repeat = true;");
            builder.AppendLine("var token1 = '';");
            builder.AppendLine("var workItemId1 = '';");
            builder.AppendLine("");
            builder.AppendLine("");
            builder.AppendLine("exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {");
            builder.AppendLine("    let action = request.body.queryResult.action;");
            builder.AppendLine("    var chat = \"here is a sample response: trump sucks\";");
            builder.AppendLine("    response.setHeader('Content-Type', 'applicaiton/json');");
            builder.AppendLine("");
            builder.AppendLine("    console.log(\"action is :\" + action);");
            builder.AppendLine("    const parameters = request.body.queryResult.parameters;");
            builder.AppendLine("    console.log(\"parameters : \" + parameters);");
            builder.AppendLine("");
            builder.AppendLine("    //ToDo: Change the code written in if-else block as per your Intent settings.");
            builder.AppendLine("    if (action === 'input.accNumber')");
            builder.AppendLine("    {");
            builder.AppendLine("        console.log(\"accNumber : \" + parameters['accNumber']);");
            builder.AppendLine("        data_template = data_template.replace(\"{1}\", parameters['accNumber']);");
            builder.AppendLine("    }");
            builder.AppendLine("    else");
            builder.AppendLine("    {");
            builder.AppendLine("        workItemId1 = parameters['reqID'];");
            builder.AppendLine("        console.log(workItemId1);");
            builder.AppendLine("    }");
            builder.AppendLine("");
            builder.AppendLine("    getToken(response, action);");
            builder.AppendLine("});");
            builder.AppendLine("");
            builder.AppendLine("");
            builder.AppendLine("function getToken(CloudFnResponse, action)");
            builder.AppendLine("{");
            builder.AppendLine("    var pathString = \"/v1/authentication\";");
            builder.AppendLine("    var data = \"{\\\"username\\\": \\\" + username + \\\", \\\"password\\\": \\\" + password + \\\"}\";");
            builder.AppendLine("");
            builder.AppendLine("    var option = {");
            builder.AppendLine("        host: controlRoomURL,");
            builder.AppendLine("        path: '/v1/authentication',");
            builder.AppendLine("        method: 'POST',");
            builder.AppendLine("        port: 443,");
            builder.AppendLine("        timeout: 30000,");
            builder.AppendLine("        headers:");
            builder.AppendLine("        {");
            builder.AppendLine("            'Content-Type': 'application/json',");
            builder.AppendLine("            'Content-Length': data.length");
            builder.AppendLine("        }");
            builder.AppendLine("    };");
            builder.AppendLine("");
            builder.AppendLine("    var request = https.request(option, function(response) {");
            builder.AppendLine("");
            builder.AppendLine("        var json = \"\";");
            builder.AppendLine("        response.on('data', function(chunk)");
            builder.AppendLine("        {");
            builder.AppendLine("            json += chunk;");
            builder.AppendLine("        });");
            builder.AppendLine("");
            builder.AppendLine("        response.on('end', function()");
            builder.AppendLine("        {");
            builder.AppendLine("            var jsonData = JSON.parse(json);");
            builder.AppendLine("            var token = JSON.stringify(jsonData.token);");
            builder.AppendLine("            token1 = token.replace(/['\"]+/g, '');");
            builder.AppendLine("");
            builder.AppendLine("            if (action === 'input.accNumber')");
            builder.AppendLine("            {");
            builder.AppendLine("                callbot(token1, CloudFnResponse);");
            builder.AppendLine("            }");
            builder.AppendLine("            else");
            builder.AppendLine("            {");
            builder.AppendLine("                getOutput(token1, workItemId1, CloudFnResponse);");
            builder.AppendLine("            }");
            builder.AppendLine("        });");
            builder.AppendLine("    });");
            builder.AppendLine("    request.on('error', (e) => {");
            builder.AppendLine("        console.error(e);");
            builder.AppendLine("    });");
            builder.AppendLine("    request.write(data);");
            builder.AppendLine("    request.end();");
            builder.AppendLine("}");
            builder.AppendLine("");
            builder.AppendLine("");
            builder.AppendLine("function callbot(token1, CloudFnResponse)");
            builder.AppendLine("{");
            builder.AppendLine("    var options = {");
            builder.AppendLine("        host: controlRoomURL,");
            builder.AppendLine("        path: '/v3/wlm/queues/' + queueID + '/workitems',");
            builder.AppendLine("        port: '443',");
            builder.AppendLine("        method: 'POST',");
            builder.AppendLine("        headers:{");
            builder.AppendLine("            'Content-Type': 'application/json',");
            builder.AppendLine("            'X-Authorization': token1");
            builder.AppendLine("        }");
            builder.AppendLine("    };");
            builder.AppendLine("");
            builder.AppendLine("    var req = https.request(options, (ressponse) =>");
            builder.AppendLine("    {");
            builder.AppendLine("        var str = '';");
            builder.AppendLine("        ressponse.on('data', function(chunk) {");
            builder.AppendLine("            str += chunk;");
            builder.AppendLine("        });");
            builder.AppendLine("        ressponse.on('end', function() {");
            builder.AppendLine("            var apiResponse = JSON.parse(str);");
            builder.AppendLine("            var workItemId = JSON.stringify(apiResponse.list[0].id);");
            builder.AppendLine("            workItemId1 = workItemId.replace(/['\"]+/g, '');");
            builder.AppendLine("");
            builder.AppendLine("");
            builder.AppendLine("            console.log(workItemId1);");
            builder.AppendLine("            CloudFnResponse.send(buildChatResponse(\"Your request has been created with request id : \" + workItemId1 + \"\\r\\n Please request again after 15 seconds with this id to get the result\"));");
            builder.AppendLine("        });");
            builder.AppendLine("    });");
            builder.AppendLine("    req.on('error', (e) => {");
            builder.AppendLine("        console.error(`Palak problem with request in addWorkItem: ${ e.message}`);");
            builder.AppendLine("    });");
            builder.AppendLine("    req.write(data_template);");
            builder.AppendLine("    req.end();");
            builder.AppendLine("}");
            builder.AppendLine("");
            builder.AppendLine("");
            builder.AppendLine("function getOutput(token1, workItemId1, CloudFnResponse)");
            builder.AppendLine("{");
            builder.AppendLine("    var options = {");
            builder.AppendLine("        host: controlRoomURL,");
            builder.AppendLine("        path: '/v3/wlm/queues/' + queueID + '/workitems/' + workItemId1,");
            builder.AppendLine("        port: '443',");
            builder.AppendLine("        method: 'GET',");
            builder.AppendLine("        headers:{");
            builder.AppendLine("            'Content-Type': 'application/json',");
            builder.AppendLine("            'X-Authorization': token1");
            builder.AppendLine("        }");
            builder.AppendLine("    };");
            builder.AppendLine("");
            builder.AppendLine("    var req = https.request(options, (ressponse) =>");
            builder.AppendLine("    {");
            builder.AppendLine("        var str = '';");
            builder.AppendLine("        ressponse.on('data', function(chunk) {");
            builder.AppendLine("            str += chunk;");
            builder.AppendLine("        });");
            builder.AppendLine("        ressponse.on('end', function() {");
            builder.AppendLine("            var apiResponse = JSON.parse(str);");
            builder.AppendLine("            var re1 = '';");
            builder.AppendLine("            re1 = JSON.stringify(apiResponse.status);");
            builder.AppendLine("            var result = JSON.stringify(apiResponse.result);");
            builder.AppendLine("");
            builder.AppendLine("            if (rel != \"\\\"COMPLETED\\\"\")");
            builder.AppendLine("            {");
            builder.AppendLine("                repeat = true;");
            builder.AppendLine("                console.log(\"Retrying....\" + re1);");
            builder.AppendLine("                //setTimeout(getOutput, 2000, token1, workItemId1, CloudFnResponse);");
            builder.AppendLine("                CloudFnResponse.send(buildChatResponse(\"Your requets is under progress, kindly wait for more 15 seconds\"));");
            builder.AppendLine("            }");
            builder.AppendLine("            else");
            builder.AppendLine("            {");
            builder.AppendLine("                repeat = false;");
            builder.AppendLine("                var result1 = result.replace(/['\"]+/g, '');");
            builder.AppendLine("");
            builder.AppendLine("                result1 = \"output is \" + result1;");
            builder.AppendLine("                console.log(\"Result found:\" + result1);");
            builder.AppendLine("                CloudFnResponse.send(buildChatResponse(result1));");
            builder.AppendLine("            }");
            builder.AppendLine("        });");
            builder.AppendLine("    });");
            builder.AppendLine("    req.on('error', (e) => {");
            builder.AppendLine("        console.log(`Palak problem with request in addWorkItem: ${ e.message}`);");
            builder.AppendLine("        console.error(`Palak problem with request in addWorkItem: ${ e.message}`); ");
            builder.AppendLine("    });");
            builder.AppendLine("    req.end();");
            builder.AppendLine("}");
            builder.AppendLine("");
            builder.AppendLine("");
            builder.AppendLine("function buildChatResponse(chat)");
            builder.AppendLine("{");
            builder.AppendLine("    return JSON.stringify({\"fulfillmentText\": chat});");
            builder.AppendLine("}");
            return builder.ToString();
        }

        private static String generateAWSLambdaCode(String controlRoomUrl, String userName, String password, String queueID, String workitemModel)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("const http = require('https');");
            builder.AppendLine($"const username = \"{userName}\";");
            builder.AppendLine($"const password = \"{password}\";");
            builder.AppendLine($"const controlroomUrl = \"{controlRoomUrl}\";");
            builder.AppendLine($"const queueId = \"{queueID}\";");
            builder.AppendLine($"var workItemTemplate = \"{workitemModel.Replace("\"", "\\\"")}\";");
            builder.AppendLine("");
            builder.AppendLine("exports.handler = async(event, context) => {");
            builder.AppendLine("");
            builder.AppendLine("    let token = await authorize();");
            builder.AppendLine("    let id = await addWorkItem(token, event);");
            builder.AppendLine("");
            builder.AppendLine("    while (1){");
            builder.AppendLine("        let result = await getOutput(token, id.id);");
            builder.AppendLine("        console.log(result.status);");
            builder.AppendLine("        if (result.status == \"COMPLETED\"){");
            builder.AppendLine("            var res = JSON.stringify(result.result);");
            builder.AppendLine("            context.succeed(JSON.parse(\"{\\\"result\\\": \" + res + \"}\"));");
            builder.AppendLine("            break;");
            builder.AppendLine("        }");
            builder.AppendLine("    }");
            builder.AppendLine("};");
            builder.AppendLine("");
            builder.AppendLine(generateCodeForAuthorize());
            builder.AppendLine("");
            builder.AppendLine(generateCodeForAddWorkItem());
            builder.AppendLine("");
            builder.AppendLine(generateCodeForGetOutput());
            return builder.ToString();
        }

        private static String generateCodeForAuthorize()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("async function authorize()");
            builder.AppendLine("{");
            builder.AppendLine("    const data = \"{\\\"username\\\": \\\"\" + username + \"\\\", \\\"password\\\": \\\"\" + password + \"\\\"}\";");
            builder.AppendLine("    const options = {");
            builder.AppendLine("      host: controlroomUrl,");
            builder.AppendLine("      path: '/v1/authentication',");
            builder.AppendLine("      port: '443',");
            builder.AppendLine("      method: 'POST',");
            builder.AppendLine("      timeout: 30000");
            builder.AppendLine("    };");
            builder.AppendLine("");
            builder.AppendLine("    const promise = new Promise(function(resolve, reject){");
            builder.AppendLine("");
            builder.AppendLine("        var req = http.request(options, (res) => {");
            builder.AppendLine("");
            builder.AppendLine("            var str = \"\";");
            builder.AppendLine("            res.on(\"data\", (chunk) => {");
            builder.AppendLine("                str += chunk;");
            builder.AppendLine("            });");
            builder.AppendLine("");
            builder.AppendLine("            res.on(\"end\", () => {");
            builder.AppendLine("                var apiResponse = JSON.parse(str);");
            builder.AppendLine("                console.log(apiResponse);");
            builder.AppendLine("                var token = JSON.stringify(apiResponse.token);");
            builder.AppendLine("                token = token.replace(/['\"]+/g, '');");
            builder.AppendLine("");
            builder.AppendLine("                resolve(token);");
            builder.AppendLine("            });");
            builder.AppendLine("");
            builder.AppendLine("        }).on(\"error\", (e) => {");
            builder.AppendLine("            reject(Error(e));");
            builder.AppendLine("        });");
            builder.AppendLine("");
            builder.AppendLine("        req.write(data);");
            builder.AppendLine("        req.end();");
            builder.AppendLine("    });");
            builder.AppendLine("");
            builder.AppendLine("    return promise;");
            builder.AppendLine("}");
            return builder.ToString();
        }

        private static String generateCodeForAddWorkItem()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("async function addWorkItem(token, args)");
            builder.AppendLine("{");
            builder.AppendLine("");
            builder.AppendLine("    var data = workItemTemplate.replace(\"{1}\", args.no);");
            builder.AppendLine("    data = data.replace(\"{2}\", args.name);");
            builder.AppendLine("    data = data.replace(\"{3}\", args.age);");
            builder.AppendLine("");
            builder.AppendLine("    const options = {");
            builder.AppendLine("        host: controlroomUrl,");
            builder.AppendLine("        path: \"/v3/wlm/queues/\" + queueId + \"/workitems\",");
            builder.AppendLine("        port: \"443\",");
            builder.AppendLine("        method: \"POST\",");
            builder.AppendLine("        headers:{");
            builder.AppendLine("            'Content-Type': 'application/json',");
            builder.AppendLine("            'X-Authorization': token");
            builder.AppendLine("        }");
            builder.AppendLine("    };");
            builder.AppendLine("");
            builder.AppendLine("    const promise = new Promise(function(resolve, reject){");
            builder.AppendLine("");
            builder.AppendLine("");
            builder.AppendLine("        var req = http.request(options, (res) => {");
            builder.AppendLine("");
            builder.AppendLine("            var str = \"\";");
            builder.AppendLine("            res.on(\"data\", (chunk) => {");
            builder.AppendLine("                str += chunk;");
            builder.AppendLine("            });");
            builder.AppendLine("");
            builder.AppendLine("            res.on(\"end\", () => {");
            builder.AppendLine("                var apiResponse = JSON.parse(str);");
            builder.AppendLine("                console.log(apiResponse);");
            builder.AppendLine("                var workItemId = JSON.stringify(apiResponse.list[0].id);");
            builder.AppendLine("                workItemId = JSON.parse(\"{\\\"id\\\": \" + workItemId + \"}\");");
            builder.AppendLine("                resolve(workItemId);");
            builder.AppendLine("            });");
            builder.AppendLine("");
            builder.AppendLine("        }).on(\"error\", (e) => {");
            builder.AppendLine("            reject(Error(e));");
            builder.AppendLine("        });");
            builder.AppendLine("");
            builder.AppendLine("        req.write(data);");
            builder.AppendLine("        req.end();");
            builder.AppendLine("    });");
            builder.AppendLine("");
            builder.AppendLine("    return promise;");
            builder.AppendLine("}");
            return builder.ToString();
        }

        private static String generateCodeForGetOutput()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("async function getOutput(token, id)");
            builder.AppendLine("{");
            builder.AppendLine("");
            builder.AppendLine("    const options = {");
            builder.AppendLine("        host: controlroomUrl,");
            builder.AppendLine("        path: \"/v3/wlm/queues/\" + queueId + \"/workitems/\" + id,");
            builder.AppendLine("        port: \"443\",");
            builder.AppendLine("        method: \"GET\",");
            builder.AppendLine("        headers:{");
            builder.AppendLine("            'Content-Type': 'application/json',");
            builder.AppendLine("            'X-Authorization': token");
            builder.AppendLine("        }");
            builder.AppendLine("    };");
            builder.AppendLine("");
            builder.AppendLine("    const promise = new Promise(function(resolve, reject){");
            builder.AppendLine("");
            builder.AppendLine("");
            builder.AppendLine("        var req = http.request(options, (res) => {");
            builder.AppendLine("");
            builder.AppendLine("            var str = \"\";");
            builder.AppendLine("            res.on(\"data\", (chunk) => {");
            builder.AppendLine("                str += chunk;");
            builder.AppendLine("            });");
            builder.AppendLine("");
            builder.AppendLine("            res.on(\"end\", () => {");
            builder.AppendLine("                var apiResponse = JSON.parse(str);");
            builder.AppendLine("                console.log(apiResponse);");
            builder.AppendLine("                var result = JSON.stringify(apiResponse.result);");
            builder.AppendLine("                result = JSON.parse(\"{\\\"result\\\": \" + result + \"}\");");
            builder.AppendLine("                resolve(result);");
            builder.AppendLine("            });");
            builder.AppendLine("");
            builder.AppendLine("        }).on(\"error\", (e) => {");
            builder.AppendLine("            reject(Error(e));");
            builder.AppendLine("        });");
            builder.AppendLine("");
            builder.AppendLine("        req.end();");
            builder.AppendLine("");
            builder.AppendLine("    });");
            builder.AppendLine("");
            builder.AppendLine("    return promise;");
            builder.AppendLine("}");
            return builder.ToString();
        }
        
        private void btnCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(rtbDFCode.Text);

        }
    }
}
