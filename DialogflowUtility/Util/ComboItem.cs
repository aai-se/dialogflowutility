﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.AutomationAnywhere.Utility.Dialogflow.Util
{
    internal class ComboItem
    {
        public String Id { get; private set; }
        public String Name { get; private set; }
        public String WorkItemId { get; private set; }

        public ComboItem(String id, String name, String workItemId)
        {
            this.Id = id;
            this.Name = name;
            this.WorkItemId = workItemId;
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
