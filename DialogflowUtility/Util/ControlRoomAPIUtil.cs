﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Com.AutomationAnywhere.Utility.Dialogflow.Util
{
    internal sealed class ControlRoomAPIUtil
    {
        static ControlRoomAPIUtil _instance;

        private static object _instanceLock = new object();

        private ControlRoomAPIUtil() { }

        public static ControlRoomAPIUtil Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_instanceLock)
                    {
                        if (_instance == null)
                        {
                            _instance = new ControlRoomAPIUtil();
                        }
                    }
                }
                return _instance;
            }
        }

        public string GetAccessToken(string controlRoomUrl, string username, string password)
        {
            string apiUrl = string.Concat(controlRoomUrl, "/v1/authentication");
            var client = new RestClient(apiUrl);

            client.Timeout = 3000;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            var data = "{\"username\": \"" + username + "\", \"password\": \"" + password + "\"}";
            request.AddParameter("application/json", data, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            if(response.StatusCode == HttpStatusCode.OK)
            {
                var jsonObject = JObject.Parse(response.Content);
                return jsonObject.GetValue("token").ToString();
            }
            return null;
        }

        public async Task<string> GetAccessTokenAsync(String controlRoomUrl, String username, String password)
        {
            string apiUrl = string.Concat(controlRoomUrl, "/v1/authentication");
            var client = new RestClient(apiUrl);

            client.Timeout = 3000;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            var data = "{\"username\": \"" + username + "\", \"password\": \"" + password + "\"}";
            request.AddParameter("application/json", data, ParameterType.RequestBody);
            IRestResponse response = await client.ExecuteAsync(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonObject = JObject.Parse(response.Content);
                return jsonObject.GetValue("token").ToString();
            }
            return null;
        }

        public IEnumerable<ComboItem> GetWorkQueues(String controlRoomUrl, String token)
        {
            string apiUrl = string.Concat(controlRoomUrl, "/v3/wlm/queues/list");
            var client = new RestClient(apiUrl);

            client.Timeout = 3000;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Authorization", token);
            var data = "{\"sort\":[{\"field\":\"name\",\"direction\":\"asc\"}]," +
                "\"filter\":{\"operator\": \"eq\", \"value\": \"IN_USE\", \"field\": \"status\"},\"fields\":[]," +
                "\"page\":{\"length\":100,\"offset\":0}}";
            request.AddParameter("application/json", data, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonObject = JObject.Parse(response.Content);
                var array = jsonObject.GetValue("list").ToArray();
                return getComboItemArray(array);
            }
            return null;
        }

        public async Task<IEnumerable<ComboItem>> GetWorkQueuesAsync(String controlRoomUrl, String token)
        {
            string apiUrl = string.Concat(controlRoomUrl, "/v3/wlm/queues/list");
            var client = new RestClient(apiUrl);

            client.Timeout = 3000;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Authorization", token);
            var data = "{\"sort\":[{\"field\":\"name\",\"direction\":\"asc\"}]," +
                "\"filter\":{\"operator\": \"eq\", \"value\": \"IN_USE\", \"field\": \"status\"},\"fields\":[]," +
                "\"page\":{\"length\":100,\"offset\":0}}";
            request.AddParameter("application/json", data, ParameterType.RequestBody);
            IRestResponse response = await client.ExecuteAsync(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonObject = JObject.Parse(response.Content);
                var array = jsonObject.GetValue("list").ToArray();
                return await getComboItemArrayAsync(array);
            }
            return null;
        }

        public string GetWorkQueueModel(String controlRoomUrl, String token, String workItemId)
        {
            string apiUrl = string.Concat(controlRoomUrl, "/v3/wlm/workitemmodels/list");
            var client = new RestClient(apiUrl);

            client.Timeout = 3000;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Authorization", token);
            var data = "{}";
            request.AddParameter("application/json", data, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonObject = JObject.Parse(response.Content);
                var array = jsonObject.GetValue("list").ToArray();
                return getWorkItemModel(array, workItemId);
            }
            return null;
        }

        public async Task<string> GetWorkQueueModelAsync(String controlRoomUrl, String token, String workItemId)
        {
            string apiUrl = string.Concat(controlRoomUrl, "/v3/wlm/workitemmodels/list");
            var client = new RestClient(apiUrl);

            client.Timeout = 3000;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Authorization", token);
            var data = "{}";
            request.AddParameter("application/json", data, ParameterType.RequestBody);
            IRestResponse response = await client.ExecuteAsync(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonObject = JObject.Parse(response.Content);
                var array = jsonObject.GetValue("list").ToArray();
                return await getWorkItemModelAsync(array, workItemId);
            }
            return null;
        }

        private static string getWorkItemModel(JToken[] array, String workItemId)
        {
            String result = String.Empty;
            foreach (JToken item in array)
            {
                String id = item.Value<String>("id");
                if (id.Equals(workItemId))
                {
                    result = getWorkItemString(item as JObject);
                }
            }
            return result;
        }

        private static async Task<String> getWorkItemModelAsync(JToken[] array, String workItemId)
        {
            return await Task.Run<String>(async () =>
            {
                String result = String.Empty;
                foreach (JToken item in array)
                {
                    String id = item.Value<String>("id");
                    if (id.Equals(workItemId))
                    {
                        result = await getWorkItemStringAsync(item as JObject);
                    }
                }
                return result;
            });
        }

        private static string getWorkItemString(JObject @object)
        {
            var attributes = @object.GetValue("attributes").ToArray();
            StringBuilder builder = new StringBuilder();
            String result = "{\"workItems\":[{\"json\":{TEMPLATE}}]}";
            int counter = 0;
            foreach (JToken attribute in attributes)
            {
                counter++;
                String attrib_name = attribute.Value<String>("name");
                String attrib_type = attribute.Value<String>("type");

                if ("NUMBER".Equals(attrib_type))
                {
                    builder.Append($", \"{attrib_name}\": {{{counter}}}");
                }
                else
                {
                    builder.Append($", \"{attrib_name}\": \"{{{counter}}}\"");
                }
            }
            return result.Replace("TEMPLATE", builder.ToString().Substring(1).Trim());
        }

        private static async Task<String> getWorkItemStringAsync(JObject @object)
        {
            return await Task.Run<String>(() => { return getWorkItemString(@object); });
        }

        private static IEnumerable<ComboItem> getComboItemArray(JToken[] array)
        {
            var result = new ComboItem[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                var token = array[i];
                result[i] = new ComboItem(token.Value<String>("id"), token.Value<String>("name"), token.Value<String>("workItemModelId"));
            }
            return result;
        }

        private static async Task<IEnumerable<ComboItem>> getComboItemArrayAsync(JToken[] array)
        {
            var t = await Task.Run<IEnumerable<ComboItem>>(() =>
            {
                return getComboItemArray(array);
            });
            return t;
        }
    }
}
